package com.diniska.nimble_signatures;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

@SuppressWarnings("serial")
public class NeembleSignaturesServlet extends HttpServlet {
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setHeader("Cache-Control", "public");
		
		String title = req.getParameter("key");
		Image image = getImage(title);

		if (image != null && image.getImage() != null) {
			resp.setContentType("image/png");
			BlobKey key = image.getImage();
			blobstoreService.serve(key, resp);
		} else {
			resp.sendRedirect("/static/noimage.jpg");
		}
	}

	private Image getImage(String key) {
		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query query = pm.newQuery(Image.class, "key == titleParam");
		query.declareParameters("String titleParam");
		query.setRange(0, 1);

		Image res = null;
		
		try {
			List<Image> results = (List<Image>) query.execute(key);
			if (results.iterator().hasNext()) {
				res = results.get(0);
			}
		} finally {
			query.closeAll();
			pm.close();
		}
		return res;
	}
}
