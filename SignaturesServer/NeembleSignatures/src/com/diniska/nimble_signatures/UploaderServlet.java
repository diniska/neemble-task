package com.diniska.nimble_signatures;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;

@SuppressWarnings("serial")
public class UploaderServlet extends HttpServlet {
	private BlobstoreService blobstoreService = BlobstoreServiceFactory
			.getBlobstoreService();

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		resp.getWriter().print("{\"url\":\"" + blobstoreService.createUploadUrl("/upload") + "\"}");
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("application/json");
		
		Map<String, List<BlobKey>> blobs = blobstoreService.getUploads(req);

		if (blobs.keySet().isEmpty()) {
			resp.getWriter().print("{\"res\":false}");
			return;
		}

		List<BlobKey> keys = blobs.get("image");
		if (keys != null) {
			BlobKey key = keys.get(0);

			PersistenceManager pm = PMF.get().getPersistenceManager();

			Image image = new Image();
			image.setImage(key);
			pm.makePersistent(image);

			resp.getWriter().print("{\"res\":true}");
		} else {
			resp.getWriter().print("{\"res\":false}");
		}
	}
}
