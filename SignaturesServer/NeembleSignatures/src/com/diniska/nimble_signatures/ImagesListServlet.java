package com.diniska.nimble_signatures;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class ImagesListServlet extends HttpServlet {
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/html");
		PrintWriter w = resp.getWriter();
		w.println("<html><head></head><body>");
		List<String> imagesKeys = getImageKeysList();
		for (String imageKey : imagesKeys) {
			w.println("<div style='display:inline-block;border:1px solid gray;margin:1em;'><img  width=\"320px\" src=\"/neemblesignatures?key=" + imageKey + "\"/></div>");
		}
		w.println("</body></html>");
	}

	private List<String> getImageKeysList() {
		PersistenceManager pm = PMF.get().getPersistenceManager();

		Query query = pm.newQuery(Image.class);

		List<String> res = null;

		try {
			List<Image> list = (List<Image>) query.execute();
			res = new ArrayList<String>(list.size());
			for (Image image : list) {
				res.add(image.getKey());
			}
		} finally {
			query.closeAll();
			pm.close();
		}
		return res;
	}
}
