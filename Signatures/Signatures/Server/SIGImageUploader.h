//
//  SIGImageUploader.h
//  Signatures
//
//  Created by Denis Chaschin on 21.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SIGTypes.h"

/**
 * Image will be uploaded to url.
 * App background mode doesn't interrupt this uploading for about 10 minutes.
 */
@interface SIGImageUploader : NSObject
+ (instancetype)uploaderForUrl:(NSString *)url image:(UIImage *)image;
- (void)upload:(void (^)(BOOL success, NSError *error))callback;
@end
