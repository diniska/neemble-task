//
//  SIGServer.m
//  Signatures
//
//  Created by Denis Chaschin on 20.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//
#import <AFNetworking/AFURLResponseSerialization.h>
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>

#import "SIGServer.h"

static NSString *const kSignatureServerUrl = @"http://neemblesignatures.appspot.com";

@implementation SIGServer

+ (instancetype)instance {
    static SIGServer *res;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[SIGServer alloc] initWithBaseURL:[NSURL URLWithString:kSignatureServerUrl]];
    });
    return res;
}

- (id)init {
    if (self = [super init]) {
        self.responseSerializer = [[AFJSONResponseSerializer alloc] init];
    }
    return self;
}

- (void)getUploadingUrl:(void (^)(NSString *url))callback
          errorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback {
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [self GET:@"upload" parameters:nil success:^(AFHTTPRequestOperation *operation, id json) {
        callback(json[@"url"]);
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorCallback) {
            errorCallback(operation, error);
        }
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    }];
}

- (void)uploadImage:(UIImage *)image
              toUrl:(NSString *)url
           callback:(CalledBlock)callback
      errorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback {
    [[AFNetworkActivityIndicatorManager sharedManager] incrementActivityCount];
    [self POSTToAbsoluteUrl:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSData *data = UIImagePNGRepresentation(image);
        [formData appendPartWithFileData:data
                                    name:@"image"
                                fileName:@"image.png"
                                mimeType:@"image/png"];
    } success:^(AFHTTPRequestOperation *operation, id json) {
        if ([json[@"res"] integerValue] == 1) {
            if (callback) {
                callback();
            }
        } else {
            if (errorCallback) {
                errorCallback(nil, nil);//TODO: add error description
            }
        }
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (errorCallback) {
            errorCallback(operation, error);
        }
        [[AFNetworkActivityIndicatorManager sharedManager] decrementActivityCount];
    }];
}

#pragma mark - Private

- (AFHTTPRequestOperation *)POSTToAbsoluteUrl:(NSString *)URLString
                      parameters:(id)parameters
       constructingBodyWithBlock:(void (^)(id <AFMultipartFormData> formData))block
                         success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    NSMutableURLRequest *request = [self.requestSerializer multipartFormRequestWithMethod:@"POST"
                                                                                URLString:URLString
                                                                               parameters:parameters
                                                                constructingBodyWithBlock:block
                                                                                    error:nil];
    AFHTTPRequestOperation *operation = [self HTTPRequestOperationWithRequest:request success:success failure:failure];
    [self.operationQueue addOperation:operation];
    return operation;
}
@end
