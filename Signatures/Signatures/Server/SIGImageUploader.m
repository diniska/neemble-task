//
//  SIGImageUploader.m
//  Signatures
//
//  Created by Denis Chaschin on 21.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "SIGImageUploader.h"
#import "SIGServer.h"

@interface SIGImageUploader()
@property (assign, nonatomic) UIBackgroundTaskIdentifier backgroundTask;
@end

@implementation SIGImageUploader {
    NSString *_url;
    UIImage *_image;
}

+ (instancetype)uploaderForUrl:(NSString *)url image:(UIImage *)image; {
    SIGImageUploader *const res = [[self alloc] init];
    res->_url = url;
    res->_image = image;
    return res;
}

- (id)init {
    if (self = [super init]) {
        _backgroundTask = UIBackgroundTaskInvalid;
    }
    return self;
}

- (void)dealloc {
    if (_backgroundTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:_backgroundTask];
    }
}

- (void)upload:(void (^)(BOOL success, NSError *error))callback {
    [self startBackgroundTask];
    
    __weak typeof(self) bself = self;
    
    [[SIGServer instance] uploadImage:_image toUrl:_url callback:^{
        if (callback) {
            callback(YES, nil);
        }
        [bself endBackgroundTask];
    } errorCallback:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (callback) {
            callback(NO, error);
        }
        [bself endBackgroundTask];
    }];
}

- (void)startBackgroundTask {
    __weak typeof(self) bself = self;
    
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [bself endBackgroundTask];
    }];
    
    DLog(@"background task started");
}

- (void)endBackgroundTask {
    [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
    self.backgroundTask = UIBackgroundTaskInvalid;
    DLog(@"background task finshed");
}
@end
