//
//  SIGServer.h
//  Signatures
//
//  Created by Denis Chaschin on 20.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import "SIGTypes.h"

@interface SIGServer : AFHTTPRequestOperationManager
+ (instancetype)instance;
- (void)getUploadingUrl:(void (^)(NSString *url))callback
          errorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback;
- (void)uploadImage:(UIImage *)image
              toUrl:(NSString *)url
           callback:(CalledBlock)callback
      errorCallback:(void (^)(AFHTTPRequestOperation *operation, NSError *error))errorCallback;
@end
