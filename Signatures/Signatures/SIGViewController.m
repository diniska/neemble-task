//
//  SIGViewController.m
//  Signatures
//
//  Created by Denis Chaschin on 19.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "SIGViewController.h"
#import "SIGSignatureView.h"
#import "SIGUploadingManager.h"

@interface SIGViewController () <SIGSignatureViewDelegate, SIGUploadingManagerDelegate>
@property (weak, nonatomic) UIPanGestureRecognizer *recognizer;
@property (strong, nonatomic) IBOutlet SIGSignatureView *signatureView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarBottomConstraint;
@end

@implementation SIGViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.signatureView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [SIGUploadingManager instance].delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [SIGUploadingManager instance].delegate = nil;
}

- (void)dealloc {
    [self.view removeGestureRecognizer:self.recognizer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buttons events
- (IBAction)clearButtonDidClick:(id)sender {
    [self.signatureView erase];
}

- (IBAction)sendButtonDidClick:(id)sender {
    UIImage *const image = self.signatureView.image;
    [[SIGUploadingManager instance] uploadSignature:image];
}

#pragma mark - SIGSignatureViewDelegate
- (void)signatureViewDidBeginEditing:(SIGSignatureView *)view {
    self.toolbarBottomConstraint.constant = -self.toolbar.frame.size.height;
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:.3 delay:.1 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.view layoutIfNeeded];
        
    } completion:nil];
}

- (void)signatureViewDidFinishEditing:(SIGSignatureView *)view {
    self.toolbarBottomConstraint.constant = 0;
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:.3 delay:.3 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
}

#pragma mark - SIGUploadingManagerDelegate
- (void)signatureUploadingManager:(SIGUploadingManager *)manager didFailUpload:(UIImage *)signature {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Unable to upload signature" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}
@end
