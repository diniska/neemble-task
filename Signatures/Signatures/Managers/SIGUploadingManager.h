//
//  SIGUploadingManager.h
//  Signatures
//
//  Created by Denis Chaschin on 20.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SIGUploadingManager;

@protocol SIGUploadingManagerDelegate <NSObject>
@optional
- (void)signatureUploadingManager:(SIGUploadingManager *)manager didUploadSignature:(UIImage *)signature;
- (void)signatureUploadingManager:(SIGUploadingManager *)manager didFailUpload:(UIImage *)signature;
@end

@interface SIGUploadingManager : NSObject
+ (instancetype)instance;
- (void)uploadSignature:(UIImage *)signature;
@property (weak, nonatomic) id<SIGUploadingManagerDelegate> delegate;
@end
