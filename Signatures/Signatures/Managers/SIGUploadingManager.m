//
//  SIGUploadingManager.m
//  Signatures
//
//  Created by Denis Chaschin on 20.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <AFNetworking/AFNetworkReachabilityManager.h>

#import "SIGUploadingManager.h"
#import "SIGServer.h"
#import "SIGImageUploader.h"

static short kMaxAttemptsNumber = 3;

@interface SIGUploadingManagerTask : NSObject
@property (strong, nonatomic) UIImage *image;
@property (assign, nonatomic) short attemptsNumber;
- (id)initWithImage:(UIImage *)image;
@end

@implementation SIGUploadingManager {
    NSMutableArray *_queue;
    NSLock *_queueLock;
    BOOL _uploadingInProgress;
    BOOL _waitingForInternetReachable;
    SIGImageUploader *_imageUploader;
}

+ (instancetype)instance {
    static SIGUploadingManager *res;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        res = [[SIGUploadingManager alloc] init];
    });
    return res;
}

- (id)init {
    if (self = [super init]) {
        _queue = [NSMutableArray array];
        _queueLock = [[NSLock alloc] init];
    }
    return self;
}

- (void)uploadSignature:(UIImage *)signature {
    [_queueLock lock];
    [_queue addObject:[[SIGUploadingManagerTask alloc] initWithImage:signature]];//TODO: save to disk if memory warnings
    [_queueLock unlock];
    [self uploadNextSignatureIfNeed];
}

#pragma mark - Private
- (void)uploadNextSignatureIfNeed {
    if (_uploadingInProgress || _waitingForInternetReachable) {
        return;
    }
    _uploadingInProgress = YES;
    [_queueLock lock];
    SIGUploadingManagerTask *nextImageToUpload = [_queue firstObject];
    if (nextImageToUpload != nil) {
        [_queue removeObjectAtIndex:0];
        [self uploadImage:nextImageToUpload];
        [_queueLock unlock];
    } else {
        _uploadingInProgress = NO;
        [_queueLock unlock];
    }
}

- (void)uploadImage:(SIGUploadingManagerTask *)task {
    __weak typeof(self) bself = self;
    [[SIGServer instance] getUploadingUrl:^(NSString *url) {
        DLog(@"uploading url received %@", url);
        [bself uploadImage:task withUrl:url];
    } errorCallback:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"fail to receive uploading url %@", error);
        [bself didFailUploadingImage:task error:error];
    }];
}

- (void)uploadImage:(SIGUploadingManagerTask *)task withUrl:(NSString *)url {
    _imageUploader = [SIGImageUploader uploaderForUrl:url image:task.image];
    __weak typeof(self) bself = self;
    [_imageUploader upload:^(BOOL success, NSError *error) {
        if (success) {
            DLog(@"photo did upload successfull");
            [bself didUploadImage:task];
        } else {
            DLog(@"photo uploading fall");
            [bself didFailUploadingImage:task error:error];
        }
    }];
}

- (void)didFailUploadingImage:(SIGUploadingManagerTask *)task error:(NSError *)error{
    if (task != nil) {
        ++task.attemptsNumber;
        if (task.attemptsNumber >= kMaxAttemptsNumber) {
            if ([self.delegate respondsToSelector:@selector(signatureUploadingManager:didFailUpload:)]) {
                [self.delegate signatureUploadingManager:self didFailUpload:task.image];
            }
        } else {
            [_queueLock lock];
            [_queue addObject:task];
            [_queueLock unlock];
        }
    }
    _uploadingInProgress = NO;
    if (error.code == NSURLErrorNotConnectedToInternet) {
        [self subscribeForReachabilityNotifications];
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    } else {
        [self uploadNextSignatureIfNeed];
    }
}

- (void)didUploadImage:(SIGUploadingManagerTask *)task {
    _uploadingInProgress = NO;
    [self uploadNextSignatureIfNeed];
    if ([self.delegate respondsToSelector:@selector(signatureUploadingManager:didUploadSignature:)]) {
        [self.delegate signatureUploadingManager:self didUploadSignature:task.image];
    }
}

#pragma mark - Reachability

- (void)subscribeForReachabilityNotifications {
    if (_waitingForInternetReachable) {
        return;
    }
    _waitingForInternetReachable = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityStatusDidChangeNotification:)
                                                 name:AFNetworkingReachabilityDidChangeNotification
                                               object:nil];
}

- (void)unsubscribeFromReachabilityNotification {
    if (!_waitingForInternetReachable) {
        return;
    }
    _waitingForInternetReachable = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AFNetworkingReachabilityDidChangeNotification
                                                  object:nil];
}

- (void)reachabilityStatusDidChangeNotification:(NSNotification *)notification {
    NSNumber *currentStatus = notification.userInfo[AFNetworkingReachabilityNotificationStatusItem];
    switch ([currentStatus integerValue]) {
        case AFNetworkReachabilityStatusReachableViaWiFi:
        case AFNetworkReachabilityStatusReachableViaWWAN: {
            [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
            [self unsubscribeFromReachabilityNotification];
            [self uploadNextSignatureIfNeed];
            break;
        }
        default:
            break;
    }
}
@end

@implementation SIGUploadingManagerTask
- (id)initWithImage:(UIImage *)image {
    if (self = [super init]){
        _image = image;
    }
    return self;
}
@end
