//
//  SIGTypes.h
//  Signatures
//
//  Created by Denis Chaschin on 21.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^CalledBlock)(void);