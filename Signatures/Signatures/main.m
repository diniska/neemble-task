//
//  main.m
//  Signatures
//
//  Created by Denis Chaschin on 19.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SIGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SIGAppDelegate class]));
    }
}
