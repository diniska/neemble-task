//
//  SIGAppDelegate.h
//  Signatures
//
//  Created by Denis Chaschin on 19.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SIGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
