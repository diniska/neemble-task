//
//  SIGSignatureView.h
//  Signatures
//
//  Created by Denis Chaschin on 19.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SIGSignatureView;

@protocol SIGSignatureViewDelegate <NSObject>
@optional
- (void)signatureViewDidBeginEditing:(SIGSignatureView *)view;
- (void)signatureViewDidFinishEditing:(SIGSignatureView *)view;
@end

@interface SIGSignatureView : UIView
@property (strong, nonatomic) UIImage *image;
/**
 * Black by default
 */
@property (strong, nonatomic) UIColor *color;
@property (weak, nonatomic) id<SIGSignatureViewDelegate> delegate;
- (void)erase;
@end
