//
//  SIGSignatureView.m
//  Signatures
//
//  Created by Denis Chaschin on 19.05.14.
//  Copyright (c) 2014 diniska. All rights reserved.
//

#import "SIGSignatureView.h"

static const CGFloat kLineWidth = 2;
static const CGFloat kPointWidth = kLineWidth * 3 / 2;
static const CGFloat kPointOffset = kPointWidth / 2;

static inline CGPoint CGPointMiddle(CGPoint first, CGPoint second);

@implementation SIGSignatureView {
    CGPoint _points[5];
    uint _ctr;
    dispatch_queue_t _drawingQueue;
    NSLock *_curvesBufferLock;
    NSMutableArray *_curvesPointsBuffer;
    BOOL _touchesWasMoved;
}
    
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    [self setMultipleTouchEnabled:NO];
    
    _drawingQueue = dispatch_queue_create("drawingQueue", DISPATCH_QUEUE_SERIAL);
    dispatch_set_target_queue(_drawingQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
    
    self.contentMode = UIViewContentModeRedraw; //to rotate correctly
    
    _color = [UIColor blackColor];
    self.backgroundColor = [UIColor whiteColor];
    
    _curvesBufferLock = [[NSLock alloc] init];
    _curvesPointsBuffer = [NSMutableArray array];
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [_image drawInRect:(CGRect){rect.origin, _image.size}];
}

- (void)erase {
    _image = [self makeDrawing:nil atImage:nil];
    [self setNeedsDisplay];
}

#pragma mark - UIResponder

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    _ctr = 0;
    _points[0] = [[touches anyObject] locationInView:self];
    if ([self.delegate respondsToSelector:@selector(signatureViewDidBeginEditing:)]) {
        [self.delegate signatureViewDidBeginEditing:self];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    _touchesWasMoved = YES;
    ++_ctr;
    _points[_ctr] = [[touches anyObject] locationInView:self];
    if (_ctr == 4) {
        _points[3] = CGPointMiddle(_points[2], _points[4]);
        
        NSMutableArray *pointsToAdd = [NSMutableArray arrayWithCapacity:4];
        for (short i = 0; i < 4; ++i) {
            [pointsToAdd addObject:[NSValue valueWithCGPoint:_points[i]]];
        }
        
        [_curvesBufferLock lock];
        [_curvesPointsBuffer addObjectsFromArray:pointsToAdd];
        [_curvesBufferLock unlock];
        
        [self performDrawingAsynchronously];
        
        _points[0] = _points[3];
        _points[1] = _points[4];
        _ctr = 1;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_touchesWasMoved) {
        _touchesWasMoved = NO;
    } else {
        CGPoint const point = [[touches anyObject] locationInView:self];
        _image = [self drawPoint:point atImage:_image];
    }
    [self setNeedsDisplay];
    if ([self.delegate respondsToSelector:@selector(signatureViewDidFinishEditing:)]) {
        [self.delegate signatureViewDidFinishEditing:self];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}

#pragma mark - Image drawing

- (void)performDrawingAsynchronously {
    dispatch_async(_drawingQueue, ^{
        if (_curvesPointsBuffer.count != 0) {
            @autoreleasepool {
                [_curvesBufferLock lock];
                NSArray *toDraw = [_curvesPointsBuffer copy];
                [_curvesPointsBuffer removeAllObjects];
                [_curvesBufferLock unlock];
                _image = [self drawCurves:toDraw atImage:_image];
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setNeedsDisplay];
        });
    });
}

- (UIImage *)drawCurves:(NSArray *)curvesPoints atImage:(UIImage *)src {
    UIBezierPath *path = [UIBezierPath bezierPath];
    for (int i = 0; i < curvesPoints.count; i += 4) {
        [path moveToPoint:[curvesPoints[i] CGPointValue]];
        [path addCurveToPoint:[curvesPoints[i+3] CGPointValue]
                controlPoint1:[curvesPoints[i+1] CGPointValue]
                controlPoint2:[curvesPoints[i+2] CGPointValue]];
    }
    [path setLineWidth:kLineWidth];
    
    return [self drawPath:path atImage:_image];
}

- (UIImage *)drawPath:(UIBezierPath *)path atImage:(UIImage *)src {
    return [self makeDrawing:^(CGContextRef context) {
        [self.color setStroke];
        [path stroke];
    } atImage:src];
}

- (UIImage *)drawPoint:(CGPoint)point atImage:(UIImage *)src {
    return [self makeDrawing:^(CGContextRef context){
        [self.color setFill];
        CGContextFillEllipseInRect(context, (CGRect){point.x - kPointOffset, point.y - kPointOffset, kPointWidth, kPointWidth});
    } atImage:src];
}

- (UIImage *)makeDrawing:(void (^)(CGContextRef context))drawingBlock atImage:(UIImage *)src {
    CGFloat const width = MAX(self.bounds.size.width, src.size.width);
    CGFloat const height = MAX(self.bounds.size.height, src.size.height);
    CGSize const size = (CGSize){width, height};
    CGRect const frame = (CGRect){CGPointZero, size};
    UIGraphicsBeginImageContextWithOptions(size, YES, 0.0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (src == nil || src.size.width < width || src.size.height < height) {
        [self.backgroundColor setFill];
        CGContextFillRect(context, frame);
    }
    [src drawAtPoint:CGPointZero];
    [self.color setFill];
    if (drawingBlock) {
        drawingBlock(context);
    }
    UIImage *res = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return res;
}


@end

static inline CGPoint CGPointMiddle(CGPoint first, CGPoint second) {
   return (CGPoint){(first.x + second.x) / 2, (first.y + second.y) / 2};
}
