package com.diniska.neemble.signature.views;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class DrawSignatureView extends View implements OnTouchListener {

	private static int DELAY_BEFORE_DRAWING_START = 100;
	private static int DELAY_BEFORE_DRAWING_FINISH = 800;
	
	private Point[] points = new Point[5];
	private short ctr;
	private List<Point> curvesPointsBuffer = new LinkedList<Point>();
	private Paint paint = new Paint();
	boolean touchesWasMoved;
	private enum DrawingStatus{
		DrawingStatusNotDrawing,
		DrawingStatusInProgress
	} ;
	private DrawingStatus previousDrawingStatusForNotifications;
	private DrawingStatus drawingStatus;

	private Bitmap bitmap;
	private Canvas canvas;

	public interface DrawSignatureViewEventsListener {
		public void onBeginDrawing(DrawSignatureView view);

		public void onEndDrawing(DrawSignatureView view);
	}

	private DrawSignatureViewEventsListener drawListener;

	public DrawSignatureView(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initialize();
	}

	public DrawSignatureView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}

	public DrawSignatureView(Context context) {
		super(context);
		initialize();
	}

	private void initialize() {
		setFocusable(true);
		setFocusableInTouchMode(true);

		this.setOnTouchListener(this);

		paint.setColor(Color.WHITE);
		paint.setStrokeWidth(2);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeCap(Paint.Cap.ROUND);
	}

	@Override
	public void onDraw(Canvas canvas) {
		canvas.drawBitmap(getBitmap(), 0, 0, null);
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		boolean res = true;
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			onTouchBegin(event);
			break;
		case MotionEvent.ACTION_MOVE:
			onTouchMoved(event);
			break;
		case MotionEvent.ACTION_UP:
			onTouchEnd(event);
			break;
		default:
			res = false;
			break;
		}
		return res;
	}

	private static class Point {
		float x, y;

		public Point(float x, float y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public String toString() {
			return x + ", " + y;
		}

		static Point middlePoint(Point first, Point second) {
			return new Point((first.x + second.x) / 2, (first.y + second.y) / 2);
		}
	}

	private void onTouchBegin(MotionEvent event) {
		drawingStatus = DrawingStatus.DrawingStatusInProgress;
		updateDrawingStateAndNotifyListener();
		ctr = 0;
		points[0] = new Point(event.getX(), event.getY());
	}

	private void onTouchMoved(MotionEvent event) {
		touchesWasMoved = true;
		++ctr;
		points[ctr] = new Point(event.getX(), event.getY());
		if (ctr == 4) {
			points[3] = Point.middlePoint(points[2], points[4]);

			List<Point> pointsToAdd = new ArrayList<Point>(4);
			for (short i = 0; i < 4; ++i) {
				pointsToAdd.add(points[i]);
			}

			curvesPointsBuffer.addAll(pointsToAdd);

			drawCurve();
			points[0] = points[3];
			points[1] = points[4];
			ctr = 1;
		}
	}

	private void onTouchEnd(MotionEvent event) {
		if (touchesWasMoved) {
			drawCurve();
			touchesWasMoved = false;
			curvesPointsBuffer.clear();
		} else {
			drawPoint(new Point(event.getX(), event.getY()));
		}
		invalidate();
		drawingStatus = DrawingStatus.DrawingStatusNotDrawing;
		updateDrawingStateAndNotifyListener();
	}

	private void drawCurve() {
		List<Point> toDraw = curvesPointsBuffer;
		curvesPointsBuffer = new LinkedList<Point>();

		final Canvas canvas = getCanvas();
		final Path p = new Path();

		for (int i = 0; i < toDraw.size(); i += 4) {
			final Point toMove = toDraw.get(i);
			final Point end = toDraw.get(i + 3);
			final Point first = toDraw.get(i + 1);
			final Point second = toDraw.get(i + 2);
			p.moveTo(toMove.x, toMove.y);
			p.cubicTo(first.x, first.y, second.x, second.y, end.x, end.y);
		}
		canvas.drawPath(p, paint);
		invalidate();
	}

	private void drawPoint(Point p) {
		Canvas canvas = getCanvas();
		canvas.drawPoint(p.x, p.y, paint);
		invalidate(Math.max((int) p.x - 1, 0), Math.max(0, (int) p.y - 1),
				Math.min((int) p.x + 1, getWidth()),
				Math.min((int) p.x + 1, getHeight()));
	}

	private Bitmap getBitmap() {
		if (bitmap == null) {
			bitmap = Bitmap.createBitmap(getWidth(), getHeight(),
					Bitmap.Config.RGB_565);
			bitmap.eraseColor(Color.rgb(0, 153, 204));
		}
		return bitmap;
	}
	
	public void clear() {
		setBitmap(null);
	}
	
	public Bitmap getImage() {
		return Bitmap.createBitmap(getBitmap());
	}

	private void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
		canvas = null;
		invalidate();
	}

	private Canvas getCanvas() {
		if (canvas == null) {
			canvas = new Canvas(getBitmap());
		}
		return canvas;
	}

	@SuppressWarnings("unused")
	private void setCanvas(Canvas canvas) {
		// readonly
	}
	
	private void updateDrawingStateAndNotifyListener() {
		if (previousDrawingStatusForNotifications == drawingStatus || drawListener == null) {
			return;
		}
		
		Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		    @Override
		    public void run() {
		    	if (previousDrawingStatusForNotifications != drawingStatus && drawListener != null) {
		    		switch (drawingStatus) {
					case DrawingStatusInProgress:
						drawListener.onBeginDrawing(DrawSignatureView.this);
						break;
					case DrawingStatusNotDrawing:
						drawListener.onEndDrawing(DrawSignatureView.this);
						break;
					}
		    		previousDrawingStatusForNotifications = drawingStatus;
		    	}
		    }

		}, drawingStatus == DrawingStatus.DrawingStatusInProgress ? DELAY_BEFORE_DRAWING_START : DELAY_BEFORE_DRAWING_FINISH);//delay
	}

	public DrawSignatureViewEventsListener getDrawListener() {
		return drawListener;
	}

	public void setDrawListener(DrawSignatureViewEventsListener listener) {
		this.drawListener = listener;
	}
}
