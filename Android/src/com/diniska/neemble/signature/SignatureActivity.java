package com.diniska.neemble.signature;

import com.diniska.neemble.signature.server.SignatureServer;
import com.diniska.neemble.signature.util.SystemUiHider;
import com.diniska.neemble.signature.views.DrawSignatureView;
import com.diniska.neemble.signature.views.DrawSignatureView.DrawSignatureViewEventsListener;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * Full-screen activity that shows and hides the system UI (i.e. status bar and
 * navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class SignatureActivity extends Activity {

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;

	private SignatureServer server = new SignatureServer();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_signature);

		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		final DrawSignatureView contentView = (DrawSignatureView) findViewById(R.id.fullscreen_content);

		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(this, contentView,
				HIDER_FLAGS);
		mSystemUiHider.setup();
		mSystemUiHider
				.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
					// Cached values.
					int mControlsHeight;
					int mShortAnimTime;

					@Override
					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
					public void onVisibilityChange(boolean visible) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
							// If the ViewPropertyAnimator API is available
							// (Honeycomb MR2 and later), use it to animate the
							// in-layout UI controls at the bottom of the
							// screen.
							if (mControlsHeight == 0) {
								mControlsHeight = controlsView.getHeight();
							}
							if (mShortAnimTime == 0) {
								mShortAnimTime = getResources().getInteger(
										android.R.integer.config_shortAnimTime);
							}
							controlsView
									.animate()
									.translationY(visible ? 0 : mControlsHeight)
									.setDuration(mShortAnimTime);
						} else {
							// If the ViewPropertyAnimator APIs aren't
							// available, simply show or hide the in-layout UI
							// controls.
							controlsView.setVisibility(visible ? View.VISIBLE
									: View.GONE);
						}
					}
				});

		contentView.setDrawListener(new DrawSignatureViewEventsListener() {
			@Override
			public void onEndDrawing(DrawSignatureView view) {
				mSystemUiHider.show();
			}

			@Override
			public void onBeginDrawing(DrawSignatureView view) {
				mSystemUiHider.hide();
			}
		});

		findViewById(R.id.clear_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						contentView.clear();
					}
				});
		findViewById(R.id.send_button).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						server.uploadSignature(contentView.getImage(),
								new SignatureServer.ErrorHandler() {
									@Override
									public void onServerError() {
										showServerErrorAlert();
									}
								});
					}
				});
	}

	private void showServerErrorAlert() {
		new AlertDialog.Builder(SignatureActivity.this).setTitle("Error")
				.setMessage("Unable to upload signature")
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.cancel();
					}
				}).create().show();
	}
}
