package com.diniska.neemble.signature.server;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

public class ServerGetJSONRequestTask extends ServerJSONRequestTask {
	private String url;

	ServerGetJSONRequestTask(String url) {
		this.url = url;
	}

	@Override
	protected HttpUriRequest createRequest() {
		HttpGet request = null;
		try {
			request = new HttpGet();
			request.setURI(new URI(url));
		} catch (URISyntaxException e) {
		}
		return request;
	}

}
