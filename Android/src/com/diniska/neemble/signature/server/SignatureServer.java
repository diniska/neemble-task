package com.diniska.neemble.signature.server;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

public class SignatureServer {

	public static interface ErrorHandler {
		public void onServerError();
	}

	static String SERVER_URL = "http://neemblesignatures.appspot.com";

	public void uploadSignature(Bitmap signature, ErrorHandler errorHandler) {
		receiveUploadingUrlAndUploadImage(signature, errorHandler);
	}

	private void receiveUploadingUrlAndUploadImage(final Bitmap image,
			final ErrorHandler errorHandler) {
		HttpAsyncTask task = new HttpAsyncTask(
				new HttpAsyncTaskResultReceiver() {
					@Override
					public void onResultReceived(JSONObject result) {
						try {
							if (result == null) {
								callErrorHandler(errorHandler);
							} else {
								uploadImage(image, result.getString("url"),
										errorHandler);
							}
						} catch (JSONException e) {
							callErrorHandler(errorHandler);
						}
					}
				});
		ServerJSONRequestTask request = new ServerGetJSONRequestTask(SERVER_URL
				+ "/upload");
		task.execute(request);
	}

	private void uploadImage(Bitmap image, String url,
			final ErrorHandler errorHandler) {
		HttpAsyncTask task = new HttpAsyncTask(
				new HttpAsyncTaskResultReceiver() {

					@Override
					public void onResultReceived(JSONObject result) {
						try {
							if (result == null) {
								callErrorHandler(errorHandler);
							} else {
								if (result.getBoolean("res") == true) {
									// successfully uploaded
								} else {
									callErrorHandler(errorHandler);
								}
							}
						} catch (JSONException e) {
							callErrorHandler(errorHandler);
						}
					}
				});
		ServerJSONRequestTask request = new ServerImageUploaderJSONRequestTask(
				url, image);
		task.execute(request);
	}

	private void callErrorHandler(ErrorHandler errorHandler) {
		if (errorHandler != null) {
			errorHandler.onServerError();
		}
	}

	private interface HttpAsyncTaskResultReceiver {
		public void onResultReceived(JSONObject result);
	}

	private class HttpAsyncTask extends
			AsyncTask<ServerJSONRequestTask, Void, JSONObject> {

		private HttpAsyncTaskResultReceiver receiver;

		public HttpAsyncTask(HttpAsyncTaskResultReceiver receiver) {
			this.receiver = receiver;
		}

		@Override
		protected JSONObject doInBackground(ServerJSONRequestTask... task) {
			task[0].run();
			JSONObject res = null;
			try {
				final String result = task[0].getResult();
				if (result != null) {
					res = new JSONObject(result);
				}
			} catch (JSONException e) {
			}
			return res;
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			if (receiver != null) {
				receiver.onResultReceived(result);
			}
		}
	}
}
