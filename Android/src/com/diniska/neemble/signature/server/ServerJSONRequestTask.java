package com.diniska.neemble.signature.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

abstract class ServerJSONRequestTask {


	private String result;

	public void run() {
		result = null;
		HttpClient httpClient = new DefaultHttpClient();
		try {
			HttpResponse response = httpClient.execute(createRequest());
			result = convertResponseToString(response);
		} catch (ClientProtocolException e) {
		} catch (IOException e) {
		}
	}

	public String getResult() {
		return result;
	}

	protected abstract HttpUriRequest createRequest();

	private String convertResponseToString(HttpResponse response)
			throws IllegalStateException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String l = "";
        String nl = System.getProperty("line.separator");
        while ((l = in.readLine()) !=null){
            sb.append(l + nl);
        }
        in.close();
        return sb.toString();
	}
}