package com.diniska.neemble.signature.server;

import java.io.ByteArrayOutputStream;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;

import android.graphics.Bitmap;

public class ServerImageUploaderJSONRequestTask extends ServerJSONRequestTask {
	private String url;
	private Bitmap image;

	public ServerImageUploaderJSONRequestTask(String url, Bitmap image) {
		this.url = url;
		this.image = image;
	}

	@Override
	protected HttpUriRequest createRequest() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		image.compress(Bitmap.CompressFormat.PNG, 90, stream); // compress to
																// which format
																// you want.
		byte[] byte_arr = stream.toByteArray();
		HttpPost httpPost = new HttpPost(url);
		ByteArrayBody bab = new ByteArrayBody(byte_arr, "image.png");

		MultipartEntity entity = new MultipartEntity(
				HttpMultipartMode.BROWSER_COMPATIBLE);

		entity.addPart("image", bab);

		httpPost.setEntity(entity);
		return httpPost;
	}

}
